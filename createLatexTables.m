%% Function Description
%
% This function will write the generated latex tables for the PBS report into a tex file
%
%% Inputs
%
% * Table: structure containing each of the latex tables generated using latexTable.
%
%% Outputs
%
% * variable: description
%
%% Change Log
%
% [1.0.0] - 2018-04-04
%
% *Fixed*
%
% * Updated to convention of Directory (folder path), Name (filename) and Path (full path to a file)
% * Option to not save to a tex directory using the structure Is. 
%
% [0.1.0] - 2017-01-01
%
% *Added*
%
% * First code

function createLatexTables(Table, fileDirectory, fileName, Is)

if Is.texDirectory
    saveDirectory = strcat(fileDirectory, '\tex');
else
    saveDirectory = fileDirectory;
end

filePath = strcat(saveDirectory, '\', fileName);

fieldNames = fieldnames(Table);
nTables = length(fieldNames);

% save LaTex code as file
fid=fopen(filePath,'w');

for iTable = 1 : nTables
    % save LaTex code as file
    
    [nrows,~] = size(Table.(fieldNames{iTable}));
    
    for row = 1:nrows
        fprintf(fid,'%s\n',Table.(fieldNames{iTable}){row,:});
    end
    
    fprintf(fid,'\n');
end

fclose(fid);
fprintf(['\n... your LaTex code has been saved as ', fileName, ' in your working directory\n']);

end

